//
// Created by mbudy on 1/16/2022.
//

#ifndef PERLIN_PERLIN_H
#define PERLIN_PERLIN_H


#include <vector>
#include <cmath>
#include <random>
#include <algorithm>
#include <numeric>

#include "vector3D.h"
#include "math.h"
#include "omp.h"

class PerlinNoise {

public:
    std::vector<int> perm;
    PerlinNoise();
    PerlinNoise(unsigned int seed);

    double noise(Vector3D<double> &v);
    math& M = math::getInstance();

    void generate_and_save(int resolution, int scale);
    void generate_and_save_threaded(int resolution, int scale);

};


#endif //PERLIN_PERLIN_H
