//
// Created by mbudy on 1/15/2022.
//

#include <cstdint>
#include "math.h"
#include "vector3D.h"

double  math::lerp(const double &am, const double &low, const double &high)
{
    return (1 - am) * low + am * high;
}


double math::fade(const double &t) {
    return t * t * t * (t * (t * 6 - 15) + 10);
}

double math::gradient(uint8_t hash, double x, double y, double z)
{
    int h = hash & 15;
    // Convert lower 4 bits of hash into 12 gradient directions
    double u = h < 8 ? x : y,
            v = h < 4 ? y : h == 12 || h == 14 ? x : z;
    return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}