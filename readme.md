The goal of this semester work was to implement Perlin noise, which is widely used not only in computer graphics to generate random terrains and other.

It's largely based on the original work of Ken Perlin, which is implemented in JAVA. Standard library is used extensively, with a "custom" Math library (singleton) implemented.

As the calculations of individual pixels are independent, threads (OpenMP) are used to speed up the process.

The resulting noise is exported as an image in PPM format.


For 2048 resolution image, on average, the single-threaded variant takes about 999ms-1318 to finish, while the multi-threaded variant finishes in 566ms-700ms.
For 1024 resolution image, on average, the single-threaded variant takes about 260ms to finish, while the multi-threaded variant finishes in 150ms.
For 4096 resolution image, on average, the single-threaded variant takes about 4.1s to finish, while the multi-threaded variant finishes in 2.2s.
