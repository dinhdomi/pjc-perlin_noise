//
// Created by mbudy on 1/16/2022.
//

#include <fstream>
#include "perlin.h"


PerlinNoise::PerlinNoise() {
    perm = {
            151,160,137,91,90,15,131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,
            8,99,37,240,21,10,23,190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,
            35,11,32,57,177,33,88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,
            134,139,48,27,166,77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,
            55,46,245,40,244,102,143,54, 65,25,63,161,1,216,80,73,209,76,132,187,208, 89,
            18,169,200,196,135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,
            250,124,123,5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,
            189,28,42,223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167,
            43,172,9,129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,
            97,228,251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,
            107,49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
            138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180 };
    // Duplicate the permutation vector
    perm.insert(perm.end(), perm.begin(), perm.end());
}

PerlinNoise::PerlinNoise(unsigned int seed) {
    perm.resize(256);
    std::iota(perm.begin(), perm.end(), 0);

    std::default_random_engine engine(seed);
    std::shuffle(perm.begin(), perm.end(), engine);
    perm.insert(perm.end(), perm.begin(), perm.end());
}

double PerlinNoise::noise(Vector3D<double> &vec) {

    // Find the unit cube that contains the point
    int X = (int) floor(vec.x) & 255;
    int Y = (int) floor(vec.y) & 255;
    int Z = (int) floor(vec.z) & 255;

    // Find relative x, y,z of point in cube
    vec.x -= floor(vec.x);
    vec.y -= floor(vec.y);
    vec.z -= floor(vec.z);

    // Compute fade curves for each of x, y, z
    double u = M.fade(vec.x);
    double v = M.fade(vec.y);
    double w = M.fade(vec.z);

    // Hash coordinates of the 8 cube corners
    int A = perm[X] + Y;
    int AA = perm[A] + Z;
    int AB = perm[A + 1] + Z;
    int B = perm[X + 1] + Y;
    int BA = perm[B] + Z;
    int BB = perm[B + 1] + Z;

    // Add blended results from 8 corners of cube
    double res = M.lerp(w,
                      M.lerp(v, M.lerp(u, M.gradient(perm[AA], vec.x, vec.y, vec.z),  M.gradient(perm[BA], vec.x-1, vec.y, vec.z)),  M.lerp(u, M.gradient(perm[AB], vec.x, vec.y-1, vec.z),  M.gradient(perm[BB], vec.x-1, vec.y-1, vec.z))),
                            M.lerp(v, M.lerp(u,  M.gradient(perm[AA+1], vec.x, vec.y, vec.z-1),  M.gradient(perm[BA+1], vec.x-1, vec.y, vec.z-1)), M.lerp(u,  M.gradient(perm[AB+1], vec.x, vec.y-1, vec.z-1), M.gradient(perm[BB+1], vec.x-1, vec.y-1, vec.z-1))));

    return (res + 1.0)/2.0;
}

void PerlinNoise::generate_and_save(int resolution, int scale) {

    const int width = resolution, height = resolution;
    auto *noiseMap = new double[width * height];

    for (int j = 0; j < height; ++j) {
        for (int i = 0; i < width; ++i) {
            double x = (double)j/width;
            double y = (double)i/height;

            auto vec = Vector3D<double>(scale * x, scale * y, 200);
            // Typical Perlin noise
            double n = noise(vec);

            noiseMap[j * width + i] = n;
        }
    }

    std::ofstream ofs;
    ofs.open("./noise2.ppm", std::ios::out | std::ios::binary);
    ofs << "P6\n" << width << " " << height << "\n255\n";
    for (unsigned k = 0; k < width * height; ++k) {
        auto n = static_cast<unsigned char>(noiseMap[k] * 255);
        ofs << n << n << n;
    }
    ofs.close();

    delete[] noiseMap;
}

void PerlinNoise::generate_and_save_threaded(int resolution, int scale) {
    const int width = resolution, height = resolution;
    auto *noiseMap = new double[width * height];

    #pragma omp parallel for shared(noiseMap, width, height)
    for (int j = 0; j < height; ++j) {
        for (int i = 0; i < width; ++i) {
            double x = (double)j/((double)width);
            double y = (double)i/((double)height);

            auto vec = Vector3D<double>(scale * x, scale * y, 200);
            // Typical Perlin noise
            double n = noise(vec);

            noiseMap[j * width + i] = n;
        }
    }

    std::ofstream ofs;
    ofs.open("./noise2.ppm", std::ios::out | std::ios::binary);
    ofs << "P6\n" << width << " " << height << "\n255\n";
    for (unsigned k = 0; k < width * height; ++k) {
        auto n = static_cast<unsigned char>(noiseMap[k] * 255);
        ofs << n << n << n;
    }
    ofs.close();

    delete[] noiseMap;
}
