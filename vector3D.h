#ifndef PERLIN_VECTOR3D_H
#define PERLIN_VECTOR3D_H

template<typename T>
class Vector3D
{
public:
    Vector3D() : x(T(0)), y(T(0)), z(T(0)) {}
    Vector3D(T x_val, T y_val, T z_val) : x(x_val), y(y_val), z(z_val) {}
    Vector3D operator * (const T &rhs) const { return Vector3D(x * rhs, y * rhs, z * rhs); }
    Vector3D& operator *= (const T &rhs) { x *= rhs, y *= rhs, z *= rhs; return *this; }
    T x, y, z;
};


#endif //PERLIN_VECTOR3D_H
