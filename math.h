//
// Created by mbudy on 1/15/2022.
//

#ifndef PERLIN_MATH_H
#define PERLIN_MATH_H

#include "vector3D.h"
class math {

public:
    static math& getInstance()
    {
        static math instance; // Guaranteed to be destroyed.
        // Instantiated on first use.
        return instance;
    }

private:
    math() {}                    // Constructor? (the {} brackets) are needed here.

public:
    math(math const&)               = delete;
    void operator=(math const&)  = delete;

    double gradient(uint8_t p, double x, double y, double z);
    double fade(const double &t);
    double lerp(const double &lo, const double &hi, const double &t);
};


#endif //PERLIN_MATH_H
