
#include <iostream>
#include <chrono>
#include <string.h>
#include "perlin.h"

template <typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

bool is_number(const std::string& s)
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}

int main(int argc, char **argv)
{

    bool useThreads = false;
    int res, scale, seed = 0;

    if(argc == 2 && strcmp(argv[1], "--help")==0) {
      std::cout << "[Perlin noise] help: " << "A simple multithreaded program to generate Perlin noise." << std::endl <<
      "Program accepts 3 (int) parameters in total: resolution, scale, and seed. Resolution controls the resolution of the image, 'scale' size of individual islands, for illustration. Seed controls the randomness" << std::endl <<
      "There is also a multi-threaded variant, to turn it on, put -threaded at the end" <<
      "Example: ./perlin 1024 4 69845222315" << std::endl;

    } else if (argc == 4 || argc == 5) {
        if (!is_number(argv[1]) || !is_number(argv[2]) || !is_number(argv[3])) {
            std::cout << "Please input a valid integer" << std::endl;
            return -1;
        }

        res = atoi( argv[1] );
        scale = atoi(argv[2]);
        seed = atoi(argv[3]);

        if (argc == 5 && strcmp(argv[4], "-threads")==0) useThreads = true;

    }  else {
        std::cout << "The parameter passed not recognized, please try again. Ending....." << std::endl;
        return -1;
    }



    PerlinNoise n(seed);

    if (useThreads) {
        auto start = std::chrono::high_resolution_clock::now();
        n.generate_and_save_threaded(res, scale);
        auto end = std::chrono::high_resolution_clock::now();
        std::cout << "Needed " << to_ms(end - start).count() << " ms to finish. THREADS\n";
    } else {
        auto start = std::chrono::high_resolution_clock::now();
        n.generate_and_save(res, scale);
        auto end = std::chrono::high_resolution_clock::now();
        std::cout << "Needed " << to_ms(end - start).count() << " ms to finish.\n";
    }

    std::cout << "Generated image saved in local directory!" << std::endl;

    return 0;
}